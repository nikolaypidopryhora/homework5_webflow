package simpleflow

class Message implements Serializable {
    String text
    static constraints = {
        text size: 5..100
    }
}
