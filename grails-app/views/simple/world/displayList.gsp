<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <meta name="layout" content="main">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Our meanings</title>
</head>
<body>
<div class="content" role="main">
    <h1>Thank you!</h1>
    Here is what other peoples think about this world:
    <table>
        <thead>
        <tr>
            <g:sortableColumn property="text" title="Meanings" />
        </tr>
        </thead>
        <tbody>
        <g:each in="${messageList}" status="i" var="message">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                <td>
                    ${fieldValue(bean: message, field: "text")}
                </td>
            </tr>
        </g:each>
        </tbody>
    </table>
</div>
</body>
</html>