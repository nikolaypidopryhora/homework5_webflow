<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <meta name="layout" content="main">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>What do you think about this world?</title>
</head>
<body>
<div class="content" role="main">
    <h1>What do you think about this world?</h1>
    <g:form action="world" method="post">
        <fieldset class="form">
            <input type="text" name='text'/>
            <g:submitButton name="submit" value="Submit"/>

            <small>We have ${count} opinions!</small>
        </fieldset>
    </g:form>
</div>
</body>
</html>