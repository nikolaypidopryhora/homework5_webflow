package simpleflow

class SimpleController {

    def index() {
        redirect(action:'world')
    }

    def worldFlow = {
        entryPoint {
            action {
                [count:Message.count()]
            }
            on("success").to "enterMessage"
        }
        enterMessage {
            on("submit") {
                def m = new Message(text:params.text)
                if(!m.validate()) return error()
                m.save()
            }.to "getList"
        }
        getList {
            action {
                [messageList:Message.list()]
            }
            on("success").to "displayList"
        }
        displayList()
    }
}
